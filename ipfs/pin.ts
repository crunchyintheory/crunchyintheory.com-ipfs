import pinataSDK from '@pinata/sdk';
import { PinataOptions } from './@pinata/sdk.d';
import { env } from 'process';
import path from 'path';
import fs from 'fs';

const pinataApiKey = env.PINATA_API_KEY;
const pinataSecretApiKey = env.PINATA_SECRET_API_KEY;

const src = './build';

const options: PinataOptions = {
    cidVersion: 1
};
    
const metadata = {
    name: env.DOMAIN
};

async function main() {

    const pinata = pinataSDK(pinataApiKey, pinataSecretApiKey);

    const pins = await pinata.pinList({ metadata: { name: env.DOMAIN } });
    const rows = pins.rows.filter(x => !x.date_unpinned);

    console.info('UNPINNING:', rows);

    for(const pin of rows) {
        await pinata.unpin(pin.ipfs_pin_hash);
    }

    const response = await pinata.pinFromFS(path.resolve(src), { pinataOptions: options, pinataMetadata: metadata });

    fs.writeFileSync(path.resolve('./hash.txt'), response.IpfsHash);
}

main();