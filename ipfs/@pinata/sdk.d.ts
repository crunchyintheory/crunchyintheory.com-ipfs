import fs from 'fs';

declare interface PinataPinPolicy {
    regions: { id: string, desiredReplicationCount: number }[];
}

declare interface PinataMetadata {
    name?: string;
    keyvalues?: { [key: string]: string };
}

declare interface PinataOptions {
    cidVersion?: number;
    wrapWithDirectory?: boolean;
    hostNodes?: string[];
    newPinPolicy?: PinataPinPolicy;
}

declare interface PinataPinOptions {
    pinataMetadata?: PinataMetadata;
    pinataOptions?: PinataOptions;
}

declare interface PinataPinListFilters {
    hashContains?: string;
    pinStart?: string;
    pinEnd?: string;
    unpinStart?: string;
    pinSizeMin?: number;
    pinSizeMax?: number;
    status?: 'all' | 'pinned' | 'unpinned';
    pageLimit?: number;
    pageOffset?: number;
    metadata?: {
        name?: string,
        keyvalues?: { [key: string]: {
            value: string,
            op: string
        } }
    };
}

declare interface PinataClient {
    hashMetadata(ipfsPinHash: string, metadata?: PinataMetadata): Promise<unknown>;
    hashPinPolicy(ipfsPinHash: string, newPinPolicy: PinataPinPolicy): Promise<unknown>; 
    pinByHash(hashToPin: string, options?: PinataPinOptions): Promise<{
        id: unknown,
        ipfsHash: string,
        status: string,
        name: string
    }>;
    pinFileToIPFS(readableStream: fs.ReadStream, options?: PinataPinOptions): Promise<{
        IpfsHash: string;
        PinSize: number;
        Timestamp: string;
    }>;
    pinFromFS(sourcePath: string, options?: PinataPinOptions): Promise<{
        IpfsHash: string;
        PinSize: number;
        Timestamp: string;
    }>;

    //

    unpin(hashToUnpin: string): Promise<string>

    //

    pinList(filters: PinataPinListFilters): Promise<{
        count: number,
        rows: {
            id: unknown,
            ipfs_pin_hash: string,
            size: number,
            user_id: unknown,
            date_pinned: string,
            date_unpinned: string,
            metadata: PinataMetadata
        }[]
    }>;
}

export default function pinataSDK(pinataApiKey: string, pinataSecretApiKey: string): PinataClient;
